#ifndef CONFIG_H
#define CONFIG_H

#define TIME_OVERSIZE           4294967296

#define PANEL_HIGH_RANGE_X      744
#define PANEL_HIGH_RANGE_Y      752
#define PANEL_LOW_RANGE_X       186
#define PANEL_LOW_RANGE_Y       189
#define PANEL_LENGHT_X          338
#define PANEL_LENGHT_Y          268
#define PANEL_PIN_UL            8
#define PANEL_PIN_UR            9
#define PANEL_PIN_LL            10
#define PANEL_PIN_LR            11
#define PANEL_PIN_AI            A0
#define PANEL_DELAY             2

#define MOTOR_PIN_X             5
#define MOTOR_PIN_Y             6
#define MOTOR_OFFSET_X          73
#define MOTOR_OFFSET_Y          92 //76
#define MOTOR_PULSE_WIDTH_MIN   550
#define MOTOR_PULSE_WIDTH_MAX   2400

#define WINDUP_ANGLE_MIN        -20.0
#define WINDUP_ANGLE_MAX        20.0

#define NODES_TOPIC_REPLY       "balancerAck"
#define NODES_TOPIC_INFO        "balancerInfo"
#define NODES_TOPIC_TUNING      "balancerTuning"


enum Code {
  FAULT = 90,
  SUCCESS = 105,
  SET_POS = 100,
  SET_P = 110,
  SET_I = 115,
  SET_D = 120,
  ACT_POS = 125,
  ACT_DEV = 130 /* Actual deviation */
};


#endif
