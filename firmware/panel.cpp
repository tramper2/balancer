#include "balancer.h"


void Panel::init() {
  calcFactor();

  pinMode(PANEL_PIN_UL, OUTPUT);
  pinMode(PANEL_PIN_UR, OUTPUT);
  pinMode(PANEL_PIN_LL, OUTPUT);
  pinMode(PANEL_PIN_LR, OUTPUT);
}
void Panel::calcFactor() {

  float cx = - (PANEL_HIGH_RANGE_X + PANEL_LOW_RANGE_X) / 2;
  float cy = - (PANEL_HIGH_RANGE_Y + PANEL_LOW_RANGE_Y) / 2;

  ax  = ( PANEL_LENGHT_X / 2 ) / ( PANEL_HIGH_RANGE_X + cx );
  ay  = ( PANEL_LENGHT_Y / 2 ) / ( PANEL_HIGH_RANGE_Y + cy );

  bx = cx * ax;
  by = cy * ay;
}
Position Panel::readVoltage() {

  Position volt;
  int help;

  digitalWrite( PANEL_PIN_UL, HIGH );
  digitalWrite( PANEL_PIN_UR, HIGH );
  digitalWrite( PANEL_PIN_LL, LOW );
  digitalWrite( PANEL_PIN_LR, LOW );

  delay( PANEL_DELAY );

  volt.y = (float)analogRead( PANEL_PIN_AI );

  digitalWrite( PANEL_PIN_UL, LOW ); //LOW
  digitalWrite( PANEL_PIN_UR, HIGH ); //HIGH
  digitalWrite( PANEL_PIN_LL, HIGH ); //HIGH
  digitalWrite( PANEL_PIN_LR, LOW ); //LOW

  delay( PANEL_DELAY );

  volt.x = (float)analogRead( PANEL_PIN_AI );

  return volt;

}
Position Panel::scale(const Position& voltage) {

  Position pos;
  int help;
  
  if( voltage.x < PANEL_LOW_RANGE_X - 30 || voltage.y < PANEL_LOW_RANGE_Y - 30 ) {
    pos.x = -10000.0;
    pos.y = -10000.0;
    return pos;
  }

  pos.x = ax * voltage.x + bx;
  pos.y = ay * voltage.y + by;


  return pos;
}
Position Panel::getPosition() {

  return scale( readVoltage() );

}
