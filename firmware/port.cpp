#include "balancer.h"


void Port::assign(const balancer::value& msg) {

  msgReply.code = Code::SUCCESS;
  msgReply.x = 0;
  msgReply.y = 0;
  
  switch ( (Code)(msg.code) ) {
    case SET_P:
      valX.P = msg.x;
      valY.P = msg.y;
      break;
    case SET_I:
      valX.I = msg.x;
      valY.I = msg.y;
      break;
    case SET_D:
      valX.D = msg.x;
      valY.D = msg.y;
      break;
    case SET_POS:
      setPos.x = Port::mapping ( msg.x );
      setPos.y = Port::mapping ( msg.y );
      break;
    default:
      msgReply.code = Code::FAULT;
      break;
  }
  
  //pubReply->publish( &msgReply );
}


void Port::init() {

  valX.P = valY.P = 0;
  valX.I = valY.I = 0;
  valX.D = valY.D = 0;
  setPos.x = 0.0;
  setPos.y = 0.0;

  pubInfo = new ros::Publisher( NODES_TOPIC_INFO, &msgInfo );
  subTuning = new ros::Subscriber<balancer::value, Port>( NODES_TOPIC_TUNING, &Port::assign, this );


  nh.initNode();
  nh.subscribe( *subTuning );
  //nh.advertise( *pubReply );
  nh.advertise( *pubInfo );

}
PID2 Port::getPID_X() {
  return valX;
}
PID2 Port::getPID_Y() {
  return valY;
}
Position Port::getSetPosition() {
  return setPos;
}
void Port::setActPosition( const Position& pos ) {
  msgInfo.x = pos.x;
  msgInfo.y = pos.y;
  msgInfo.code = ACT_POS;
  msgInfo.header.stamp = nh.now();
  //this->actPos = pos;
}
void Port::refresh() {
  pubInfo->publish( &msgInfo );
  nh.spinOnce();
  // !!! wysyłanie publsherów
}

void Port::setMsg(const float& msg) {
  msgInfo.y = msg;
}

double Port::mapping( double pos ) {
  
  int mapped_value;
  pos = pos / 5;
  mapped_value = ( int ) ( pos );
  mapped_value *= 5;
  return ( double )( mapped_value );
}
