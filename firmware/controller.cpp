#include "balancer.h"

void Controller::init(const double& antiWindupMin, const double& antiWindupMax) {
  interval();
  val.P = 0.0;
  val.I = 0.0;
  val.D = 0.00;
  yp = yi = yd = yd_prev = y_prev = 0.0;
  prevDev = 0;
  prevPos = 0;
  inertionRatio = 1.0;
  cumDiff = 0;

  this->antiWindupMin = antiWindupMin;
  this->antiWindupMax = antiWindupMax;
}
void Controller::setPID( const PID2& pid ) {
  this->val = pid;
}
double Controller::calculate( const double& actPos, const double& setPos ) {
  // val.D = 1;
  int eps = 5;
  double dt = (double)interval() / 1000.0;

  if( actPos > -1000.0 ) {
    dev =  setPos - actPos;

    //if( -eps < dev && dev < eps ) {
      //return y;
    //}

    yp = dev * val.P;
  double Td = 0;
    if(val.P != 0) {
      Td = val.D / val.P;
    }

    double tau = 0.6;
    //yd = (dev - prevDev) * val.D / dt; //last version
//    yd = (y - yd_prev) * val.D * 1000000/ dt;
//    Serial.print("val.D: ");
//    Serial.println(val.D);
//    Serial.println(yd);

    //yd = yd_prev + ( Td * (dev - prevDev) / ( tau * dt ) - ( yd_prev / tau ) ) * dt ;
    yd = ( (actPos - y_prev)/dt + yd_prev * 0.3 );
    yd_prev = yd;
    y_prev = actPos;
    yd = yd * val.D;

    // Serial.print("y: ");
	// Serial.print(y);
	// Serial.print("   y_prev: ");
	// Serial.print(y_prev);
	// Serial.print("    yd: ");
	// Serial.print(yd);
	// Serial.print("   yd_prev: ");
	// Serial.print(yd_prev);
	// Serial.print("   dt: ");
	// Serial.println(dt);
	// Serial.print("   val.D: ");
	// Serial.println(val.D);
	//
    /*if( ( -5 < yd && yd < 5 ) && ( -8 < dev && dev < 8 ) ) {
      return y;
    }*/
    if( ( antiWindupMax > (yp + yi + yd) ) && (yp + yi + yd) > antiWindupMin ) {
      yi += 0.5 * (dev + prevDev) * val.I * dt;
    }


  y = yp + yi - yd;
//  y = yi - yp - yd;

  prevPos = actPos;
  prevDev = dev;
  } else {

    y = 0.0;
    prevPos = 0.0;
    prevDev = 0.0;
    yi = 0.0;
  }



  return y;
}

unsigned long Controller::interval() {

  unsigned long interval = 0;

  //ct = micros();
  ct = millis();

  if ( lt < ct ) {
    interval = TIME_OVERSIZE - lt + ct;
  } else {
    interval = ct - lt;
  }

  lt = ct;
  return interval;
}

double Controller::getY() {
 return y;
}

double Controller::getYd() {
  return yd;
}

void Controller::clearIntegral() {
  yd_prev = 0;
  yi = 0.0;
}
