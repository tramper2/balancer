#ifndef BALANCER_H
#define BALANCER_H


#include "config.h"
#include <Arduino.h>
#include <Servo.h>
#include <ros.h>
#include <balancer/value.h>

struct Position {
  double x;
  double y;
} typedef Position;


struct PID2 {
  double P;
  double I;
  double D;
} typedef PID2;

class Controller {
  private:
    PID2 val;
    float antiWindupMin;
    float antiWindupMax;
    unsigned long ct;
    unsigned long lt;
    double prevPos;
    double prevDev;
    double dev;

    double yp, yd, yd_prev, y_prev;
    double y;
    double yi;

    double cumDiff;
    double inertionRatio;
    
    
    unsigned long interval();


  public:

    void init( const double& antiWindupMin, const double& antiWindupMax );
    void setPID( const PID2& pid );
    void clearIntegral();
    double calculate( const double& actPos, const double& setPos );
    double getY();
    double getYd();
};

class Panel {
  private:
    float ax;
    float ay;
    float bx;
    float by;

    void calcFactor();
    Position readVoltage();
    Position scale( const Position& voltage );

  public:
    void init();
    Position getPosition();

};

class Motor {
  private:
    char pin;
    float offset;
    float scale;
    Servo motor;


    void calcFactor();
    int calcMotor( const float& angle );

  public:

    void init( const char& pin, const float& offset);
    void setPlate( const float& angle );
  


};

class Port {
  private:

    PID2 valX;
    PID2 valY;
    Position actPos;
    Position setPos;
    ros::NodeHandle nh;

    balancer::value msgReply;
    balancer::value msgTuning;
    balancer::value msgInfo;

    ros::Publisher* pubInfo;
    ros::Subscriber<balancer::value, Port>* subTuning;

    void assign( const balancer::value& msg );

  public:

    void init();
    PID2 getPID_X();
    PID2 getPID_Y();
    Position getSetPosition();
    void setActPosition( const Position& pos );
    void refresh();
    void setMsg(const float& msg);
    static double mapping( double pos );

};

class kalmanFilter {

  private:
  float varMeasurement;
  float varProcess;
  float Pc;
  float G;
  float P;
  float Xp;
  float Zp;
  float Xe;
  
  public:
  
  void init();
  float calculate( float pos );
    
  
};



#endif
