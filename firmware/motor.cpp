#include "balancer.h"


void Motor::init( const char& pin, const float& offset) {
  calcFactor();
  this->offset = offset;
  motor.attach(pin);
}
void Motor::calcFactor() {
  scale = MOTOR_PULSE_WIDTH_MAX - MOTOR_PULSE_WIDTH_MIN;
  scale = scale / 180;
  

}

int Motor::calcMotor( const float& angle ) {
  return (int)( MOTOR_PULSE_WIDTH_MIN + scale * (angle + offset) );
}

void Motor::setPlate( const float& angle ) {

  
  if( angle > WINDUP_ANGLE_MAX ) {
      motor.writeMicroseconds( calcMotor( WINDUP_ANGLE_MAX ) );
  } else if ( angle < WINDUP_ANGLE_MIN ) {
      motor.writeMicroseconds( calcMotor( WINDUP_ANGLE_MIN ) );
  } else {

  
    motor.writeMicroseconds( calcMotor( angle ) );
    //motor.write( calcMotor( angle ) );

  }
  
  //Serial.print("Angle: ");
  //Serial.println( motor.read() );

}
