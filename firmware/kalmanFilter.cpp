#include "balancer.h"

float kalmanFilter::calculate( float pos ) {

  Pc = P + varProcess;
  G = Pc / ( Pc + varMeasurement );    // kalman gain
  P = ( 1 - G ) * Pc;
  Xp = Xe;
  Zp = Xp;
  Xe = G * ( pos - Zp ) + Xp;

  return Xe;
  
}

void kalmanFilter::init() {
  varMeasurement = 1.7;  // variance determined using excel and reading samples of raw sensor data
  varProcess = 1e-2;
  Pc = 0.0;
  G = 0.0;
  P = 1.0;
  Xp = 0.0;
  Zp = 0.0;
  Xe = 0.0;
}
