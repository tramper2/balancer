#include "balancer.h"
#include "config.h"
 
 
Port com;
Panel panel;
Motor motorX;
Motor motorY;
Controller controlX;
Controller controlY;


 
double argX;
double argY;
Position prevGoodPos;
int checkIfNoise = 0;
Position actPos;
Position setPos;
Position avgPosition[4];

double output_x;
double output_y;
double setpoint_x = 0;
double setpoint_y = 0;

kalmanFilter filterX, filterY;

int pos;



void setup()
{
  Serial.begin(9600);
  panel.init();
  motorX.init( MOTOR_PIN_X, MOTOR_OFFSET_X);
  motorY.init( MOTOR_PIN_Y, MOTOR_OFFSET_Y);
  controlX.init( WINDUP_ANGLE_MIN, WINDUP_ANGLE_MAX );
  controlY.init( WINDUP_ANGLE_MIN, WINDUP_ANGLE_MAX );
  com.init();

  filterX.init();
  filterY.init();
  
}



 
void loop()
{

  

  
  controlX.setPID( com.getPID_X() );
  controlY.setPID( com.getPID_Y() );

  avgPosition[0] =  avgPosition[1];
  avgPosition[1] =  avgPosition[2];
  avgPosition[2] =  avgPosition[3];
  avgPosition[2] = panel.getPosition();
  actPos.x = (avgPosition[0].x + avgPosition[1].x + avgPosition[2].x + avgPosition[3].x) / 4;
  actPos.y = (avgPosition[0].y + avgPosition[1].y + avgPosition[2].y + avgPosition[3].y) / 4;
  //actPos = panel.getPosition();
  
  setPos = com.getSetPosition();
  
  if( actPos.x > -1000.0 && actPos.y > -1000.0) {
    
    argX = controlX.calculate( actPos.x, setPos.x );
    argY = controlY.calculate( actPos.y, setPos.y );
    prevGoodPos.x = actPos.x;
    prevGoodPos.y = actPos.y;
    checkIfNoise = 0;
  } else {

    checkIfNoise += 1;
    if ( checkIfNoise < 15 ) {
      actPos.x = prevGoodPos.x;
      actPos.y = prevGoodPos.y;
      argX = controlX.calculate( actPos.x, setPos.x );
      argY = controlY.calculate( actPos.y, setPos.y );
    } else {
      argX = 0;
      argY = 0;
      controlX.clearIntegral();
      controlY.clearIntegral();
    }
  }
  
  motorX.setPlate( argX );
  motorY.setPlate( -argY );
 
  com.setActPosition( actPos );
 
  com.refresh();
//  delay(8);
  
}
