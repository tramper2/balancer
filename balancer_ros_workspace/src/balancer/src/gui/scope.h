#pragma once
#include <vector>

#include "struct.h"

class Scope {
	friend class Chart;
	friend class ToCSV;
protected:
	const char* name;
	std::vector<Vec2f> data;
	Color color;

public:
	Scope( const char* name );
	void set_color( const Color& color );
	Color get_color();
	size_t get_size();
	void add_point( const Vec2f& p );
	void drop();

	void fill();

};
