#pragma once


#define ICON_SIZE 				30
#define ICON_PATH_ONPAN_OFF 	"./res/icon_onpan-off.png"
#define ICON_PATH_ONPAN_ON 		"./res/icon_onpan-on.png"
#define ICON_PATH_DATA_OFF 		"./res/icon_save-off.png"
#define ICON_PATH_DATA_ON 		"./res/icon_save-on.png"
#define ICON_PATH_SETTINGS 		"./res/icon_settings.png"
#define ICON_PATH_CHART 		"./res/icon_chart.png"

#define OUT_BOUND 				-10000
#define PAN_LEN_X 				356
#define PAN_LEN_Y 				288
#define AIM_SIZE 				10

#define CHART_AMP_MIN 			-155
#define CHART_AMP_MAX 			155
#define CHART_BUFFER_SIZE 		300
#define CHART_COLOR_1			cairo_set_source_rgba (cr, 1, 0, 0, 1 );
#define CHART_COLOR_2			cairo_set_source_rgba (cr, 0, 1, 0, 1 );

#define TOPIC_NAME_ACK    		"balancerAck"
#define TOPIC_NAME_INFO   		"balancerInfo"
#define TOPIC_NAME_TUNING 		"balancerTuning"
#define NODE_NAME         		"GUI_Node"
