#include "signal.h"
//#include "/home/krystian/Balancer/balancer_ros_workspace/src/tests/src/talker.cpp"

// void Signal::onSwitchEnable( GtkSwitch* button, gboolean state, gpointer pdata ) {
//
//
// 	fprintf(stderr, "onSwitchEnable: Work\n" );
// 	gtk_switch_set_active( button, state );
// }
void Signal::onSwitchOnAir( GtkSwitch* button, gboolean state, gpointer pdata ) {


	fprintf(stderr, "onSwitchOnAir: Work\n" );
	gtk_switch_set_active( button, state );
}
void Signal::onClickSettings( GtkButton* button, gpointer pdata ) {


	fprintf(stderr, "onClickSettings: Work\n" );
}
void Signal::onChangeXP( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_P, 'X', value );

	fprintf(stderr, "onChangeXP: Work\n" );
}
void Signal::onChangeXI( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_I, 'X', value );


	fprintf(stderr, "onChangeXI: Work\n" );
}
void Signal::onChangeXD( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_D, 'X', value );

	fprintf(stderr, "onChangeXD: Work\n" );
}
void Signal::onChangeYP( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_P, 'Y', value );

	fprintf(stderr, "onChangeYP: Work\n" );
}
void Signal::onChangeYI( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_I, 'Y', value );

	fprintf(stderr, "onChangeYI: Work\n" );
}
void Signal::onChangeYD( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_D, 'Y', value );


	fprintf(stderr, "onChangeYD: Work\n" );
}
void Signal::onChangePosX( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_POS, 'X', value );

	fprintf(stderr, "onChangePosX: Work\n" );
}
void Signal::onChangePosY( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble value = gtk_adjustment_get_value( adjust );
	it->ros.valueAdjust( SET_POS, 'Y', value );

	fprintf(stderr, "onChangePosY: Work\n" );
}
void Signal::onClickSend( GtkButton* button, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	it->ros.send();

	fprintf(stderr, "onClickSend: Work2\n" );
}
void Signal::onChangeTimeRec( GtkAdjustment *adjust, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;
	gdouble max_time_rec = gtk_adjustment_get_value( adjust );
	gtk_adjustment_set_upper( it->adjust_progress, max_time_rec );

	fprintf(stderr, "onChangeTimeRec: Work\n" );
}
void Signal::onClickSaveData( GtkButton* button, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;

	if( it->status_data == true && it->status_record == false ) {
		it->set_status_data(false);

		GtkWidget *dialog;
		GtkFileChooser *chooser;
		gint res;

		dialog = gtk_file_chooser_dialog_new (
			"Save File",
			GTK_WINDOW(it->window),
			GTK_FILE_CHOOSER_ACTION_SAVE,
			("_Cancel"),
			GTK_RESPONSE_CANCEL,
			("_Save"),
			GTK_RESPONSE_ACCEPT,
			NULL
		);

		chooser = GTK_FILE_CHOOSER(dialog);
		gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

		res = gtk_dialog_run( GTK_DIALOG( dialog ) );
		if (res == GTK_RESPONSE_ACCEPT)
		{
			gchar* filename = gtk_file_chooser_get_filename(chooser);
			if( it->toCSV.save( filename ) == true ) {
				it->toCSV.scope_clear();
			}
			g_free (filename);
		}

		gtk_widget_destroy (dialog);
	}

	fprintf(stderr, "onClickSaveData: Work\n" );
}
void Signal::onToggleChart( GtkToggleButton* button, gpointer pdata ) {
	if( pdata == NULL ) {
		return;
	}
	Interface* it = (Interface*)pdata;

	if( gtk_toggle_button_get_active(button) == true ) {
		delete it->area;
		it->area = NULL;
		it->chart = new Chart( it->sheet );
	} else if( it->area == NULL ) {
		delete it->chart;
		it->chart = NULL;
		it->area = new Stalker( it->sheet );
	}

	fprintf(stderr, "onClickCallChart: Work\n" );
}
