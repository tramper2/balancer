#pragma once
#include <gtk/gtk.h>


class Drawable {
public:
	typedef gboolean(*pdraw)(GtkWidget*, cairo_t*, gpointer );
private:
	gulong sid_draw;
	GtkWidget* drawarea;
	pdraw draw;
public:
	Drawable( GtkWidget* drawarea, pdraw draw );
	~Drawable();

};
