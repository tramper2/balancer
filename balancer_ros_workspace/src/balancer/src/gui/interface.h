#pragma once
#include <gtk/gtk.h>
#include <glib.h>


#include "signal.h"
#include "config.h"
#include "stalker.h"
#include "chart.h"
#include "toCSV.h"
#include "../rosConnect.h"

class Interface {
	friend class Signal;
private:
	Stalker* area;
	Chart* chart;
	ToCSV toCSV;
	Scope scope_x;
	Scope scope_y;
	RosConnect ros;
	// std::string nodeName;
	bool status_data;
	bool status_onpan;
	bool status_record;
protected:
	GtkWidget* window;
	GtkBuilder* build_app;
	/* ToolBar */
	// GtkWidget* switch_enable;
	GtkWidget* switch_onair;
	GtkWidget* light_onpan;
	GtkWidget* light_ROS;
	GtkWidget* button_settings;
	GtkWidget* icon_settings;
	/* Control */
	GtkAdjustment* adjust_XP;
	GtkAdjustment* adjust_XI;
	GtkAdjustment* adjust_XD;
	GtkAdjustment* adjust_YP;
	GtkAdjustment* adjust_YI;
	GtkAdjustment* adjust_YD;
	GtkAdjustment* adjust_X;
	GtkAdjustment* adjust_Y;
	GtkWidget* button_send;
	/* Info */
	GtkWidget* label_setpos;
	GtkWidget* label_actpos;
	GtkWidget* label_deviation;
	/* Record */
	GtkAdjustment* adjust_record_range;
	GtkAdjustment* adjust_progress;
	GtkWidget* slider_progress;
	GtkWidget* light_data;
	GtkWidget* button_save_data;
	GtkWidget* toggle_call_chart;
	GtkWidget* icon_chart;
	/* Draw */
	GtkWidget* sheet;
	/* Icon */
	GdkPixbuf* pix_onpan_on;
	GdkPixbuf* pix_onpan_off;
	GdkPixbuf* pix_data_on;
	GdkPixbuf* pix_data_off;
	GdkPixbuf* pix_settings;
	GdkPixbuf* pix_chart;
private:
	void set_status_onpan( const bool state );
	void set_status_data( const bool state );
	void set_status_record( const bool state );
	static gboolean refresh( gpointer pdata );
	inline bool out_of_bound( const double& pos_x, const double& pos_y );

public:
	Interface( int argc, char** argv );

	~Interface();
	/*
	TODO:
	- Zerowanie czasu po zapisaniu (adjust_progress set value 0)
	- przygotowanie do następnego nagrywania( rosConnect::initTime = 0)
	- niszczenie danych jakimś przyciskiem
	- nieniszczenie danych jeśli nie zapiszesz
	- przypomnienie o wysłąniu
	- zatrzymanie/wznowienie nagrywania

	*/
};
