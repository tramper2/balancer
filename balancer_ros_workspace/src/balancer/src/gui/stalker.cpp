#include "stalker.h"

#include <iostream>

Stalker::Stalker( GtkWidget* drawarea ) : Drawable( drawarea, draw ) {
	this->pos.x = -10000.0;
	this->pos.y = -10000.0;
	platform.x = -PAN_LEN_X / 2;
	platform.y = -PAN_LEN_Y / 2;
	platform.w = PAN_LEN_X;
	platform.h = PAN_LEN_Y;
}
Stalker::~Stalker() {

}
void Stalker::set_target( const Vec2f& target ) {
	this->pos = target;
}
gboolean Stalker::draw(GtkWidget *widget, cairo_t *cr, gpointer pdata) {
	if( pdata == NULL ) {
		return true;
	}
	Stalker* self = (Stalker*)pdata;
	resize( widget, cr, pdata );

	cairo_rectangle(
		cr,
		self->pan.x,
		self->pan.y,
		self->pan.w,
		self->pan.h
	);
	cairo_stroke (cr);

	Vec2i target;
	if( hitarea( self->platform, self->pos ) == true ) {if( hitarea( self->platform, self->pos ) == true ) {
		target.x = (self->pos.x - self->platform.x) * self->scale.x + self->pan.x; // ???
		target.y = (self->pos.y - self->platform.y) * self->scale.y + self->pan.y; // ???
		//target.y = self->pan.h - target.y;
	} else {
		target.y = self->pan.y - 10;
		target.x = self->pan.x - 10;
	}
		target.x = (self->pos.x - self->platform.x) * self->scale.x + self->pan.x; // ???
		target.y = (self->pos.y - self->platform.y) * self->scale.y + self->pan.y; // ???
		//target.y = self->pan.h - target.y;
	} else {
		target.y = self->pan.y - 10;
		target.x = self->pan.x - 10;
	}
	viewfinder( cr, target );

	return false;
}
gboolean Stalker::resize(GtkWidget *widget, cairo_t *cr, gpointer pdata) {
	if( pdata == NULL ) {
		return true;
	}
	Stalker* self = (Stalker*)pdata;

	guint width = gtk_widget_get_allocated_width (widget);
	guint height = gtk_widget_get_allocated_height (widget);

	self->pan.x = width * 0.05;
	self->pan.y = height * 0.05;
	self->pan.w = width - self->pan.x * 2;
	self->pan.h = height - self->pan.y * 2;;


	self->scale.x = 1.0 * self->pan.w / self->platform.w;
	self->scale.y = 1.0 * self->pan.h / self->platform.h;

	return false;
}
gboolean Stalker::viewfinder(cairo_t *cr, const Vec2i& target) {

	cairo_set_source_rgb( cr, 100, 100, 10 );

	cairo_move_to( cr, (target.x - AIM_SIZE), target.y );
	cairo_rel_line_to( cr, (AIM_SIZE * 2), 0 );

	cairo_move_to( cr, target.x, (target.y - AIM_SIZE) );
	cairo_rel_line_to( cr, 0, (AIM_SIZE * 2) );

	cairo_stroke (cr);
	cairo_move_to( cr, target.x, target.y );
	cairo_set_source_rgb( cr, 0, 0, 0 );

	return false;
}
bool Stalker::hitarea( const Rect& rect, const Vec2f& point ) {
	if( point.x < rect.x ) {
		return false;
	}
	if( point.x > rect.x + rect.w ) {
		return false;
	}
	if( point.y < rect.y ) {
		return false;
	}
	if( point.y > rect.y + rect.h ) {
		return false;
	}

	return true;
}
