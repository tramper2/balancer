#pragma once
#include <gtk/gtk.h>
#include "interface.h"

class Signal {
	friend class Interface;
protected:

	// static void onSwitchEnable( GtkSwitch* button, gboolean state, gpointer pdata );
		// - change flag enable
	static void onSwitchOnAir( GtkSwitch* button, gboolean state, gpointer pdata );
		// - change flag on air
	static void onClickSettings( GtkButton* button, gpointer pdata );
		// - do nothing
	static void onChangeXP( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe P val for axis X
	static void onChangeXI( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe I val for axis X
	static void onChangeXD( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe D val for axis X
	static void onChangeYP( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe P val for axis Y
	static void onChangeYI( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe I val for axis Y
	static void onChangeYD( GtkAdjustment *adjust, gpointer pdata );
		// - chanfe D val for axis Y
	static void onChangePosX( GtkAdjustment *adjust, gpointer pdata );
		// - change setup pos X
	static void onChangePosY( GtkAdjustment *adjust, gpointer pdata );
		// - change setup pos Y
	static void onClickSend( GtkButton* button, gpointer pdata );
		// - send data to firmware
	static void onChangeTimeRec( GtkAdjustment *adjust, gpointer pdata );
		// - set max record time
	static void onClickSaveData( GtkButton* button, gpointer pdata );
		// - open dialog
		// - save as csv
	static void onToggleChart( GtkToggleButton* button, gpointer pdata );
		// - draw out in draw area


};
