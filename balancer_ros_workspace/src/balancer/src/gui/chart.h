#pragma once
#include <gtk/gtk.h>

#include "config.h"
#include "struct.h"
#include "drawable.h"

class Chart : public Drawable {
private:
	Rect workspace;
	double buf[2][CHART_BUFFER_SIZE];
	guint step;
	guint scale;
private:
	static gboolean draw(GtkWidget *widget, cairo_t *cr, gpointer pdata);
	static gboolean resize(GtkWidget *widget, cairo_t *cr, gpointer pdata);
	static gboolean rescale();
	static void axis(GtkWidget *widget, cairo_t *cr, gpointer pdata);
	static gboolean plot(GtkWidget *widget, cairo_t *cr, gpointer pdata);
public:
	Chart( GtkWidget* drawarea );
	~Chart();
	void send_pos( Vec2f pos );

};
