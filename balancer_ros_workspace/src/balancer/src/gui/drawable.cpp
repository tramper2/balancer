#include "drawable.h"

#include <iostream>

Drawable::Drawable( GtkWidget* drawarea, pdraw draw ) {
	this->drawarea = drawarea;
	this->draw = draw;
	sid_draw = g_signal_connect(drawarea, "draw", G_CALLBACK (draw), this);
	gtk_widget_queue_draw(drawarea);
}

Drawable::~Drawable() {
	if( g_signal_handler_is_connected( drawarea, sid_draw ) == true ) {
		g_signal_handler_disconnect(drawarea, sid_draw );
	}
}
