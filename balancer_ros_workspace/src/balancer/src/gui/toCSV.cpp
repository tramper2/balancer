#include "toCSV.h"

bool ToCSV::save( const char* path ) {
	file.open( path, std::ios::out );

	if( file.good() == false ) {
		std::cerr << "couldn't create file: " << path << '\n';
		return false;
	}
	if( data.size() == false ) {
		std::cerr << "no scope connected"<< '\n';
		return false;
	}

	for( auto it : data ) {
		file << it->name << ";" << ";";
	}
	file << std::endl;

	for( auto it : data ) {
		file << "x" << ";" << "y" << ";";
	}
	file << std::endl;

	for( size_t i = 0; i < max_size( data ) ; i++ ) {
		for( auto it : data ) {
			write( it, i);
		}
		file << std::endl;
	}
	file << std::endl;



	file.close();

	return true;
}
size_t ToCSV::max_size( const std::vector<Scope*> scope ) {
	if( scope.size() < 1 ) {
		return 0;
	}

	size_t max = scope[0]->data.size();
	for( auto sc : scope ) {
		if( sc->data.size() > max ) {
			max = sc->data.size();
		}
	}

	return max;
}
void ToCSV::write( const Scope* sc, size_t nr ) {
	if( nr < sc->data.size() ) {
		/* Notattion ',' */
		std::string x = std::to_string( sc->data[nr].x );
		std::string y = std::to_string( sc->data[nr].y );
		file << x << ";" << y << ";";

		/* Notattion '.' */
		// file << sc->data[nr].y << ";" << sc->data[nr].y << ";";
	} else {
		file << ";" << ";";
	}
}
void ToCSV::add_scope( Scope* scope ) {
	data.push_back( scope );
}
void ToCSV::scope_clear() {
	for( auto scope : data ) {
		scope->drop();
	}
}
