#pragma once

typedef struct Vec2i {
	int x;
	int y;
	Vec2i()
	: x(-1), y(-1) {}
	Vec2i( const int& x, const int& y )
	: x(x), y(y) {}
} Vec2i;

typedef struct Vec2f {
	double x;
	double y;
	Vec2f()
	: x(-1.0), y(-1.0) {}
	Vec2f( const double& x, const double& y )
	: x(x), y(y) {}
} Vec2f;

typedef struct Rect {
	int x;
	int y;
	int w;
	int h;
	Rect()
	: x(0), y(0), w(0), h(0) {}
	Rect( const int& x, const int& y, const int& w, const int& h )
	: x(x), y(y), w(w), h(h) {}
} Rect;

typedef struct RBGA {
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
	RBGA()
	: r(255), g(255), b(255), a(0) {}
	RBGA( const int& r, const int& g, const int& b, const int& a )
	: r(r), g(g), b(b), a(a) {}
} Color;

typedef struct MsgRos {
	// long int sec;
	// long int nsec;
	double timestamp;
	char code;
	float x;
	float y;
} MsgRos;

typedef struct Msg {

	float x;
	float y;
	bool change;

} Msg;

enum Code {
  FAULT = 90,
  SUCCESS = 105,
  SET_POS = 100,
  SET_P = 110,
  SET_I = 115,
  SET_D = 120,
  ACT_POS = 125,
  ACT_DEV = 130 /* Actual deviation */
};
