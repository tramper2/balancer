#include "scope.h"


Scope::Scope( const char* name ) : name(name) {

}
void Scope::set_color( const Color& color ) {
	this->color = color;
}
Color Scope::get_color() {
	return color;
}
void Scope::add_point( const Vec2f& p ) {
	data.push_back( p );
}
void Scope::drop() {
	data.clear();
}
size_t Scope::get_size() {
	return data.size();
}


void Scope::fill() {
	for( float i = 0; i < 100; i += 0.31 ) {
		Vec2f p( i, i*0.46 );
		data.push_back( p );
	}
}
