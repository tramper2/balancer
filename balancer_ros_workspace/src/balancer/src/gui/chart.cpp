#include "chart.h"

Chart::Chart( GtkWidget* drawarea ) : Drawable( drawarea, draw ) {

	for( int i = 0; i < CHART_BUFFER_SIZE; i++ ) {
		buf[0][i] = CHART_AMP_MIN;
		buf[1][i] = CHART_AMP_MIN;
	}

}
Chart::~Chart() {

}
gboolean Chart::draw(GtkWidget *widget, cairo_t *cr, gpointer pdata) {
	if( pdata == NULL ) {
		return true;
	}
	Chart* self = (Chart*)pdata;

	if( resize( widget, cr, pdata ) == true) {
		return true;
	}

	axis( widget, cr, pdata );
	plot( widget, cr, pdata );


	return false;
}
gboolean Chart::resize(GtkWidget *widget, cairo_t *cr, gpointer pdata) {
	if( pdata == NULL ) {
		return true;
	}
	Chart* self = (Chart*)pdata;

	guint width = gtk_widget_get_allocated_width (widget);
	guint height = gtk_widget_get_allocated_height (widget);

	self->workspace.x = width * 0.01;
	self->workspace.y = height * 0.01;
	self->workspace.w = width - self->workspace.x * 2;
	self->workspace.h = height - self->workspace.y * 2;

	self->step = self->workspace.w / (CHART_BUFFER_SIZE - 1);

	if( self->step <= 0 ) {
		return true;
	}

	self->scale = self->workspace.h / ( CHART_AMP_MAX - CHART_AMP_MIN );

	if( self->scale <= 0 ) {
		return true;
	}


	return false;
}
void Chart::axis(GtkWidget *widget, cairo_t *cr, gpointer pdata) {
	if( pdata == NULL ) {
		return;
	}
	Chart* self = (Chart*)pdata;

	cairo_move_to( cr, self->workspace.x, self->workspace.y );
	cairo_rel_line_to( cr, 0, self->workspace.h );
	cairo_rel_line_to( cr, self->workspace.w, 0 );

	cairo_stroke(cr);

	cairo_move_to( cr, self->workspace.x, self->workspace.y );
	cairo_rel_line_to( cr, 5, 10 );
	cairo_rel_line_to( cr, -10, 0 );
	cairo_close_path (cr);

	cairo_fill(cr);

	cairo_move_to( cr, self->workspace.x + self->workspace.w, self->workspace.y + self->workspace.h );
	cairo_rel_line_to( cr, -10, 5 );
	cairo_rel_line_to( cr, 0, -10 );
	cairo_close_path (cr);

	cairo_fill(cr);

}

gboolean Chart::plot( GtkWidget *widget, cairo_t *cr, gpointer pdata ) {

	if( pdata == NULL ) {
		return true;
	}
	Chart* self = (Chart*)pdata;

	for( int n = 0; n < 2; n++ ) {
		if( n == 0 ) {
			CHART_COLOR_1
		} else if( n == 1) {
			CHART_COLOR_2
		}


		int x = self->workspace.x;
		int y = self->workspace.h - self->buf[n][0] * self->scale;

		cairo_move_to( cr, x, y );
		for( int i = 1; i < CHART_BUFFER_SIZE; i++ ) {
			x += self->step;
			y = self->workspace.h - self->buf[n][i] * self->scale;
			cairo_line_to( cr, x, y );
		}
		cairo_stroke(cr);
	}

	return false;
}

void Chart::send_pos( Vec2f pos ) {
	for ( int i = 0; i < CHART_BUFFER_SIZE - 1; i++) {
		buf[0][i] = buf[0][i + 1];
		buf[1][i] = buf[1][i + 1];
	}
	buf[0][CHART_BUFFER_SIZE - 1] = pos.x - CHART_AMP_MIN;
	buf[1][CHART_BUFFER_SIZE - 1] = pos.y - CHART_AMP_MIN;
}
