#pragma once
#include <gtk/gtk.h>

#include "config.h"
#include "struct.h"
#include "drawable.h"


class Stalker : public Drawable {
private:
	Rect workspace;
	Rect platform;
	Rect pan;
	Vec2f scale;
	Vec2f pos;
private:
	static bool hitarea( const Rect& rect, const Vec2f& point );
	static gboolean draw(GtkWidget *widget, cairo_t *cr, gpointer pdata);
	static gboolean resize(GtkWidget *widget, cairo_t *cr, gpointer pdata);
	static gboolean viewfinder(cairo_t *cr, const Vec2i& target);
public:
	Stalker( GtkWidget* drawarea );
	~Stalker();
	void set_target( const Vec2f& target );
};
