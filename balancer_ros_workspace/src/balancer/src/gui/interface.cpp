#include "interface.h"

Interface::Interface( int argc, char** argv ) :
scope_x( "Actual position x" ),
scope_y( "Actual position y" )
{

	gtk_init( &argc, &argv );
	build_app = gtk_builder_new_from_file( "./res/app.glade" );
	window = GTK_WIDGET( gtk_builder_get_object( build_app, "window" ) );

	/* ToolBar */
	// switch_enable 		= GTK_WIDGET( gtk_builder_get_object( build_app, "SwitchEnable" ) );
	switch_onair 		= GTK_WIDGET( gtk_builder_get_object( build_app, "SwitchOnAir" ) );
	light_onpan 		= GTK_WIDGET( gtk_builder_get_object( build_app, "StatusOnPan" ) );
	light_ROS 			= GTK_WIDGET( gtk_builder_get_object( build_app, "StatusROS" ) );
	button_settings 	= GTK_WIDGET( gtk_builder_get_object( build_app, "ButtonSettings" ) );
	icon_settings 		= GTK_WIDGET( gtk_builder_get_object( build_app, "ImgIconSettings" ) );
	/* Control */
	adjust_XP 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustXP" ) );
	adjust_XI 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustXI" ) );
	adjust_XD 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustXD" ) );
	adjust_YP 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustYP" ) );
	adjust_YI 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustYI" ) );
	adjust_YD 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustYD" ) );
	adjust_X 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustPosX" ) );
	adjust_Y 			= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustPosY" ) );
	button_send 		= GTK_WIDGET( gtk_builder_get_object( build_app, "ButtonSend" ) );
	/* Info */
	label_setpos 		= GTK_WIDGET( gtk_builder_get_object( build_app, "LabelSetPos" ) );
	label_actpos 		= GTK_WIDGET( gtk_builder_get_object( build_app, "LabelActPos" ) );
	label_deviation 	= GTK_WIDGET( gtk_builder_get_object( build_app, "LabelDeviation" ) );
	/* Record */
	adjust_record_range = GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustTimeRec" ) );
	adjust_progress 	= GTK_ADJUSTMENT( gtk_builder_get_object( build_app, "AdjustProgress" ) );
	slider_progress 	= GTK_WIDGET( gtk_builder_get_object( build_app, "ProgressBar" ) );
	light_data 		= GTK_WIDGET( gtk_builder_get_object( build_app, "StatusData" ) );
	button_save_data 	= GTK_WIDGET( gtk_builder_get_object( build_app, "ButtonSaveData" ) );
	toggle_call_chart 	= GTK_WIDGET( gtk_builder_get_object( build_app, "ToggleCallChart" ) );
	icon_chart 			= GTK_WIDGET( gtk_builder_get_object( build_app, "ImgIconChart" ) );
	/* Draw */
	sheet 				= GTK_WIDGET( gtk_builder_get_object( build_app, "DrawArea" ) );

	/* Load Icons */
	GError *err = NULL;

	pix_onpan_off = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_ONPAN_OFF,
		ICON_SIZE*2,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	pix_onpan_on = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_ONPAN_ON,
		ICON_SIZE*2,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	pix_data_on = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_DATA_ON,
		ICON_SIZE,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	pix_data_off = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_DATA_OFF,
		ICON_SIZE,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	pix_settings = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_SETTINGS,
		ICON_SIZE,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	pix_chart = gdk_pixbuf_new_from_file_at_scale(
		ICON_PATH_CHART,
		ICON_SIZE,
		ICON_SIZE,
		TRUE,
		&err
	);
	if( err != NULL ) {
		fprintf(stderr, "ERROR [%d]: %s\n", err->code, err->message );
		err = NULL;
	}

	/*Set default icons */
	set_status_onpan( false );
	set_status_data( false );
	gtk_image_set_from_pixbuf( GTK_IMAGE(icon_settings), pix_settings );
	gtk_image_set_from_pixbuf( GTK_IMAGE(icon_chart), pix_chart );

	/* Disabling */
	gtk_widget_set_sensitive( slider_progress, FALSE );

	/*Setup position constrains */
	gtk_adjustment_set_upper( adjust_X, PAN_LEN_X/2 );
	gtk_adjustment_set_upper( adjust_Y, PAN_LEN_Y/2 );
	gtk_adjustment_set_lower( adjust_X, -PAN_LEN_X/2 );
	gtk_adjustment_set_lower( adjust_Y, -PAN_LEN_Y/2 );

	/* window */
	g_signal_connect( window, "delete-event", G_CALLBACK(gtk_main_quit), NULL );
	/* ToolBar */
	//g_signal_connect( switch_enable, "state-set", G_CALLBACK(Signal::onSwitchEnable), this );
	g_signal_connect( switch_onair, "state-set", G_CALLBACK(Signal::onSwitchOnAir), this );
	g_signal_connect( button_settings, "clicked", G_CALLBACK(Signal::onClickSettings), this );
	/* Control */
	g_signal_connect( adjust_XP, "value-changed", G_CALLBACK(Signal::onChangeXP), this );
	g_signal_connect( adjust_XI, "value-changed", G_CALLBACK(Signal::onChangeXI), this );
	g_signal_connect( adjust_XD, "value-changed", G_CALLBACK(Signal::onChangeXD), this );
	g_signal_connect( adjust_YP, "value-changed", G_CALLBACK(Signal::onChangeYP), this );
	g_signal_connect( adjust_YI, "value-changed", G_CALLBACK(Signal::onChangeYI), this );
	g_signal_connect( adjust_YD, "value-changed", G_CALLBACK(Signal::onChangeYD), this );
	g_signal_connect( adjust_X, "value-changed", G_CALLBACK(Signal::onChangePosX), this );
	g_signal_connect( adjust_Y, "value-changed", G_CALLBACK(Signal::onChangePosY), this );
	g_signal_connect( button_send, "clicked", G_CALLBACK(Signal::onClickSend), this );
	/* Record */
	g_signal_connect( adjust_record_range, "value-changed", G_CALLBACK( Signal::onChangeTimeRec), this );
	g_signal_connect( button_save_data, "clicked", G_CALLBACK(Signal::onClickSaveData), this );
	g_signal_connect( toggle_call_chart, "toggled", G_CALLBACK(Signal::onToggleChart), this );

	/*Attach class*/
	chart = NULL;
	area = new Stalker( sheet );

	toCSV.add_scope( &scope_x );
	toCSV.add_scope( &scope_y );

	//ros.init(NODE_NAME);
	gdk_threads_add_idle( this->refresh, this );

	/*Attach class*/


	gtk_widget_show_all( window );
	gtk_main();
}
Interface::~Interface() {
	delete area;
	delete chart;
}
void Interface::set_status_data( const bool state ) {
	if( state == true ) {
		gtk_image_set_from_pixbuf( GTK_IMAGE(light_data), pix_data_on );
	} else {
		gtk_image_set_from_pixbuf( GTK_IMAGE(light_data), pix_data_off );
	}
	this->status_data = state;
}
void Interface::set_status_onpan( const bool state ) {
	if( state == true ) {
		gtk_image_set_from_pixbuf( GTK_IMAGE(light_onpan), pix_onpan_on );
	} else {
		gtk_image_set_from_pixbuf( GTK_IMAGE(light_onpan), pix_onpan_off );
	}
	this->status_onpan = state;
}
void Interface::set_status_record( const bool state ) {
	this->status_record = state;
}
bool Interface::out_of_bound( const double& pos_x, const double& pos_y ) {
	if( pos_x == OUT_BOUND ) {
		return true;
	}
	if( pos_y == OUT_BOUND ) {
		return true;
	}
	return false;
}
gboolean Interface::refresh( gpointer pdata ) {
	if( pdata == NULL ) {
		return false;
	}
	Interface* it = (Interface*)pdata;
	MsgRos ros_msg;

	ros::spinOnce();
	if( it->ros.checkIfNew() == true ) {
		ros_msg = it->ros.getActState();

		/**/
		char text_setup[64];
		char text_actual[64];
		char text_deviation[64];
		Msg setup = it->ros.getSetPosition();
		if( it->out_of_bound( ros_msg.x, ros_msg.y ) == false ) {
			it->set_status_onpan( true );
			it->set_status_record( true );

			sprintf( text_setup, "%0.2fx%0.2f", setup.x, setup.y );
			sprintf( text_actual, "%0.2fx%0.2f", ros_msg.x, ros_msg.y );
			sprintf( text_deviation, "%0.2fx%0.2f", ros_msg.x - setup.x, ros_msg.y - setup.y );
		} else {
			it->set_status_onpan( false );

			sprintf( text_setup, "--x--" );
			sprintf( text_actual, "--x--" );
			sprintf( text_deviation, "--x--" );
		}
		gtk_label_set_text( GTK_LABEL( it->label_setpos), text_setup );
		gtk_label_set_text( GTK_LABEL( it->label_actpos), text_actual );
		gtk_label_set_text( GTK_LABEL( it->label_deviation), text_deviation );
		/**/

		/**/
		if( it->area != NULL ) {
			it->area->set_target( Vec2f( ros_msg.x, ros_msg.y ) );
			gtk_widget_queue_draw( it->sheet );
		}

		if( it->chart != NULL ) {
			it->chart->send_pos( Vec2f( ros_msg.x, ros_msg.y ) );
			gtk_widget_queue_draw( it->sheet );
		}
		/**/

		/**/
		if( ros_msg.timestamp >= gtk_adjustment_get_value( it->adjust_record_range ) ) {
			it->set_status_record( false );
		}
		/**/

		/**/
		if( it->status_record == true ) {

			it->scope_x.add_point( Vec2f( ros_msg.timestamp, ros_msg.x ) );
			it->scope_y.add_point( Vec2f( ros_msg.timestamp, ros_msg.y ) );
			gtk_adjustment_set_value( it->adjust_progress, ros_msg.timestamp );
			if( it->status_data == false ) {
				it->set_status_data( true );
			}
		}
		/**/


		/**/
		if( gtk_switch_get_active( GTK_SWITCH(it->switch_onair) ) == true ) {
			it->ros.send();
		}
		/**/


	}

	return true;
}
