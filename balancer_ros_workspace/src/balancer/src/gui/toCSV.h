#pragma once
#include <fstream>
#include <iostream>
#include <string>

#include "struct.h"
#include "scope.h"

class ToCSV {
private:
	std::fstream file;
	std::vector<Scope*> data;
private:
	inline void write( const Scope* sc, size_t nr );
	inline size_t max_size( const std::vector<Scope*> scope );
public:
	bool save( const char* path );
	void add_scope( Scope* scope );
	void scope_clear();
};
