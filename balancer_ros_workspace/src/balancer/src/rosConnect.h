#include "ros/ros.h"
#include "std_msgs/String.h"
#include "balancer/value.h"
#include "gui/struct.h"
#include "gui/config.h"

#include <sstream>

class RosConnect {

	public:
		RosConnect();
		void send();
		void changeValue();
		void getPos();
		void valueAdjust(Code code, char axis, float value);
		void subscriberCallback( const balancer::value::ConstPtr& msg );
		MsgRos getActState();
		Msg getSetPosition();
		bool checkIfNew();


	private:

		ros::NodeHandle* nodeHandle;
		ros::Publisher pubBalancerTuning;
		ros::Subscriber subBalancerInfo;
		balancer::value rosMsg;

		Msg kp;
		Msg ki;
		Msg kd;
		Msg pos;
		Msg setPos;
		MsgRos actState;
		double initTime;
		bool newMsg;

};
