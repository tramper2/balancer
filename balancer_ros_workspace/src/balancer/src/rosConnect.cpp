#include "rosConnect.h"
#include <iostream>


RosConnect::RosConnect( ) :
initTime(0.0) {

	std::map<std::string, std::string> remaps;
	ros::init(remaps, NODE_NAME, (uint32_t)0);
	nodeHandle = new ros::NodeHandle();
	pubBalancerTuning = nodeHandle->advertise<balancer::value>(TOPIC_NAME_TUNING, 1000);
	subBalancerInfo = nodeHandle->subscribe<balancer::value>(TOPIC_NAME_INFO, 1000, &RosConnect::subscriberCallback, this);
	newMsg = 0;

}

// void RosConnect::init( std::string node_name ) {
//
// 	std::map<std::string, std::string> remaps;
// 	ros::init(remaps, node_name, (uint32_t)0);
// 	nodeHandle = new ros::NodeHandle();
// 	pubBalancerTuning = nodeHandle->advertise<warsztaty::value>(TOPIC_NAME_TUNING, 1000);
//
// 	subBalancerInfo = nodeHandle->subscribe<warsztaty::value>(TOPIC_NAME_INFO, 1000, &RosConnect::subscriberCallback, this);
//
// }


void RosConnect::valueAdjust(Code code, char axis, float value) {

	switch (code) {

		case SET_POS:

			if( axis == 'X' ) {
				pos.x = value;
				pos.change = 1;
			} else if( axis == 'Y' ) {
				pos.y = value;
				pos.change = 1;
			} break;


		case SET_P:

			if( axis == 'X' ) {
				kp.x = value;
				kp.change = 1;
			} else if( axis == 'Y' ) {
				kp.y = value;
				kp.change = 1;
			} break;

		case SET_I:

			if( axis == 'X' ) {
				ki.x = value;
				ki.change = 1;
			} else if( axis == 'Y' ) {
				ki.y = value;
				ki.change = 1;
			} break;

		case SET_D:

			if( axis == 'X' ) {
				kd.x = value;
				kd.change = 1;
			} else if( axis == 'Y' ) {
				kd.y = value;
				kd.change = 1;
			} break;

	}

}

void RosConnect::send(){

	if( pos.change ) {
		rosMsg.code = SET_POS;
		rosMsg.x = pos.x;
		rosMsg.y = pos.y;
		pubBalancerTuning.publish(rosMsg);
		setPos = pos;
	}

	if( kp.change ) {
		rosMsg.code = SET_P;
		rosMsg.x = kp.x;
		rosMsg.y = kp.y;
		pubBalancerTuning.publish(rosMsg);
	}

	if( ki.change ) {
		rosMsg.code = SET_I;
		rosMsg.x = ki.x;
		rosMsg.y = ki.y;
		pubBalancerTuning.publish(rosMsg);
	}

	if( kd.change ) {
		rosMsg.code = SET_D;
		rosMsg.x = kd.x;
		rosMsg.y = kd.y;
		pubBalancerTuning.publish(rosMsg);
	}

	pos.change = 0;
	kp.change = 0;
	ki.change = 0;
	kd.change = 0;
}


void RosConnect::subscriberCallback( const balancer::value::ConstPtr& msg) {
	if( initTime == 0 ) {
		initTime = (double)(msg->header.stamp.sec + (double)msg->header.stamp.nsec / 1000000000);
	}

	std::cout.precision(10);
	//std::cout << " callback	 " << std::endl;
	//fprintf(stderr, "subscriber Recived Msg\n" );
	actState.x = msg->x;
	actState.y = msg->y;
	actState.timestamp = (double)(msg->header.stamp.sec + (double)msg->header.stamp.nsec / 1000000000) - initTime;
	//std::cout << "actState.timestamp	 " << actState.timestamp << std::endl;
	newMsg = 1;

}

MsgRos RosConnect::getActState() {
	newMsg = 0;
	return actState;
}

bool RosConnect::checkIfNew() {
	return newMsg;
}


Msg RosConnect::getSetPosition() {
	return setPos;
}
