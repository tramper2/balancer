
(cl:in-package :asdf)

(defsystem "balancer-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "value" :depends-on ("_package_value"))
    (:file "_package_value" :depends-on ("_package"))
  ))