# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "balancer: 1 messages, 0 services")

set(MSG_I_FLAGS "-Ibalancer:/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg;-Irosserial_msgs:/home/krystian/Balancer/balancer_ros_workspace/src/rosserial/rosserial_msgs/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(balancer_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_custom_target(_balancer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "balancer" "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(balancer
  "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/balancer
)

### Generating Services

### Generating Module File
_generate_module_cpp(balancer
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/balancer
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(balancer_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(balancer_generate_messages balancer_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_dependencies(balancer_generate_messages_cpp _balancer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(balancer_gencpp)
add_dependencies(balancer_gencpp balancer_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS balancer_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(balancer
  "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/balancer
)

### Generating Services

### Generating Module File
_generate_module_eus(balancer
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/balancer
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(balancer_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(balancer_generate_messages balancer_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_dependencies(balancer_generate_messages_eus _balancer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(balancer_geneus)
add_dependencies(balancer_geneus balancer_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS balancer_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(balancer
  "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/balancer
)

### Generating Services

### Generating Module File
_generate_module_lisp(balancer
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/balancer
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(balancer_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(balancer_generate_messages balancer_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_dependencies(balancer_generate_messages_lisp _balancer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(balancer_genlisp)
add_dependencies(balancer_genlisp balancer_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS balancer_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(balancer
  "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/balancer
)

### Generating Services

### Generating Module File
_generate_module_nodejs(balancer
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/balancer
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(balancer_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(balancer_generate_messages balancer_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_dependencies(balancer_generate_messages_nodejs _balancer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(balancer_gennodejs)
add_dependencies(balancer_gennodejs balancer_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS balancer_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(balancer
  "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/balancer
)

### Generating Services

### Generating Module File
_generate_module_py(balancer
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/balancer
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(balancer_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(balancer_generate_messages balancer_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/krystian/Balancer/balancer_ros_workspace/src/balancer/msg/value.msg" NAME_WE)
add_dependencies(balancer_generate_messages_py _balancer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(balancer_genpy)
add_dependencies(balancer_genpy balancer_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS balancer_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/balancer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/balancer
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET rosserial_msgs_generate_messages_cpp)
  add_dependencies(balancer_generate_messages_cpp rosserial_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(balancer_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/balancer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/balancer
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET rosserial_msgs_generate_messages_eus)
  add_dependencies(balancer_generate_messages_eus rosserial_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(balancer_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/balancer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/balancer
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET rosserial_msgs_generate_messages_lisp)
  add_dependencies(balancer_generate_messages_lisp rosserial_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(balancer_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/balancer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/balancer
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET rosserial_msgs_generate_messages_nodejs)
  add_dependencies(balancer_generate_messages_nodejs rosserial_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(balancer_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/balancer)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/balancer\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/balancer
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET rosserial_msgs_generate_messages_py)
  add_dependencies(balancer_generate_messages_py rosserial_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(balancer_generate_messages_py std_msgs_generate_messages_py)
endif()
