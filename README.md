# Balancer

### Spis treści
* [Wstęp](#Wstęp)
	* [Cel i zakres projektu](#Cel-i-zakres-projektu)
	* [Opis urządzenia](#Opis-urządzenia)
	* [Zastosowanie](#Zastosowanie)
		* [Dydaktyczne](#Dydaktyczne)
		* [Militarne](#Militarne)
	* [Analiza rozwiązań](#Analiza-rozwiązań)
		* [Mechanika](#Mechanika)
		* [Elementy wykonawcze](#Elementy-wykonawcze)
		* [Czujnik](#Czujnik)
		* [Regulator](#Regulator)
		* [Automatyzacja ustawiania nastaw i zbierania pomiarów](#Automatyzacja-ustawiania-nastaw-i-zbierania-pomiarów)
	* [Założenia prototypu](#Założenia-prototypu)
	* [Harmonogram zatwierdzony i wykonany](#Harmonogram)
	* [Podział obowiązków](#Podział-obowiązków)
	* [Instrukcja/Integracja](#Instrukcja/Integracja)
	* [Parametry](#Parametry)
* [Projekt](#Projekt)
	* [Zasada działania](#Zasada-działania)
	* [Graf działania](#Graf-działania)
	* [Model matematyczny](#Model-matematyczny)
	* [Spis elementów i kosztorys](#Spis-elementów-i-kosztorys)
* [Opis elementów](#Opis-elementów)
	* [Panel rezystacyjn](#Panel-rezystacyjn)
		* [Budowa paneli rezystacyjnych](#Budowa-paneli-rezystacyjnych)
		* [Działanie Paleli rezystacyjnych](#Działanie-Paleli-rezystacyjnych)
		* [Kompensacja nieliniowości w panelach 5-pinowych](#Kompensacja-nieliniowości-w-panelach-5-pinowych)
		* [Implematacja odczytu](#Implematacja-odczytu)
	* [Arduino Uno Rev3](#Arduino-Uno-Rev3)
		* [Możliwości mikrokontrolera](#Możliwości-mikrokontrolera)
	* [Serwomechanizmy](#Serwomechanizmy)
		* [Zasada działania](#Zasada-działania)
		* [Pinout](#Pinout)
* [Hardware](#Hardware)
	* [Schemat elektroniczny](#Schemat-elektroniczny)
	* [BOM](#BOM)
* [Firmware](#Firmware)
	* [Robot Operating System](#Robot-Operating-System)
		* [Nodes](#Nodes)
		* [Master](#Master)
		* [Messages](#Messages)
		* [Topics](#Topics)
		* [Services](#Services)
	* [Regulator PID](#Regulator-PID)
* [Software](#Software)
	* [Diagram klass](#Diagram-klass)
* [Napotkane problemy](#Napotkane-problemy)
* [Podsumowanie](#Podsumowanie)
* [Autorzy](#Autorzy)
* [Przypisy](#Przypisy)


# Wstęp

### Opis urządzenia
Jest to urządzenie do sterowania położeniem kulki na ruchomej platformie. Platforma reaguje na zakłócenia, które starają się zmieniać pozycje kulki. Należało wybrać elementy wykonawcze, mechanikę, regulator, sposób automatyzacji zbierania pomiarów oraz czujnik położenia. Zdecydowano się na zastosowanie serwomechanizmów, regulatora PID, przegubu cardana, panelu rezystancyjnego oraz frameworka ROS. Problematyka ta została opisana w [analizie rozwiązań](#Analiza-rozwiązań).


### Zastosowanie
#### Dydaktyczne
Przedewszystkim urządzenie ma zastosowanie dydaktyczne, służące do nauki dobierania nastaw regulatora PID, poznanie jak poszczególne człony wpływają na regulację i zachowanie układu. stworzenie modelu matematcznego, ruchomej płyty, uzmysłowienie sobie jak zmiany parametrów mechanicznych modelu wpływa na zmianę nastaw.

#### Militarne
W helikopterach typu UH, HH, Bell, takich jak UH-1 Huey lub HH-1K Iroquois. Do sterowania platformą załadunkową. Tak by ustawiana była horyzontalnie. Dzięki temu jednostki w helikopterze nie odczują tak bardzo gwałtwonych przechyleń helikoptera. Wyeliminuje uciążliwośc utrzymywania równowagi podczas, gdy jednostki na platformie prowadzą obserwacje terenu.

W radarach wojskowych montowanych na pojazdach, ponieważ pojazd zawsze stoi na nierównej powierzchni. Radar wykrywa samoloty i helikoptery w odległości nawet do 3km i każde, nawet minimalne przechylenie radaru względem powierzchni ziemii może powodować bardzo duży błąd. Wobec tego ważne jest, by ten radar ustawić horyzontalnie, bez żadnych przechyleń. Wykorzystuje się specjalne siłowniki oraz bardzo dokładne czujniki. Idealne ustawienie nie jest i tak możliwe, ale na podstawie znajomości odchyleń błąd jest dodatkowo korygowany programowo.

*W obu przypadkach wielkością sterowaną nie jest położenie kulki, lecz przechylenie, a czujnikiem nie jest panel rezystancyjny lecz żyroskop.*

### Analiza rozwiązań

#### Mechanika
Płyta powinna mieć dwa stopnie swobody - obrót wokół osi x oraz wokół osi y, tak by możliwe były jej przechylenia w celu regulacji położenia kulki. Zapewnić to mogły przegub kulowy lub przegub Cardana. Zdecydowano się na wykorzystanie przegubu Cardana, ze względu na mniejszy koszt.

#### Elementy wykonawcze
Do sterowania platformą należało wybrać odpowiedni element wykonawczy, który pozwalałby na łatwe sterowanie platformą. Rozważano następujące rozwiązania:
- silniki krokowe
- silniki dc
- silniki BLDC ze śmigłami (miały być umieszczone na rogach platformy)
- serwomechanizmy modelarskie

Silniki krokowe zapewniałyby dużą rozdzielczość sterowania kątem platformy, jednak niezbędne są dodatkowe sterowniki. Sam koszt silników krokowych może być stosunkowo duży wobec zakładanego kosztu całkowitego projektu. Rozważano wykorzystanie zwykłych silników DC, które jednak pracują w pętli otwartej. Problem mogłoby stanowić ustawienie platformy w położeniu poziomym kiedy kulka nie znajdowałaby się na niej. Konieczny mógłby okazać się żyroskop który mierzyłby nachylenie platformy. Możliwe było wykorzystanie silników BLDC z śmigłami, które umieszczone byłyby na rogach platformy. To rozwiązanie miałoby jednak kilka istotnych wad. Podobnie jak w przypadku silników DC potrzebny byłby żyroskop. Dodatkowo należałoby zakupić odpowiednie regulatory. Kolejny problem stanowiłby wygląd projektu - który przypominałby drona. Użytkownik musiałby również uważać na śmigła - by ich nie dotknąć. Należałoby zapewnić odpowiednie mocowanie podstawy by urządzenie nie oderwało się od stołu.
Zdecydowano się zastosować serwomechanizmy modelarskie, które składają się z silnika DC, przekładni, potencjometru oraz posiadają elektroniczny układ sterujący w pętli zamkniętej. Jest to najtańsze rozwiązanie, jest także łatwe w implementacji. Sterować serwomechanizmami można wykorzystując mikrokontroler posiadający wyjścia PWM.

#### Czujnik

Konieczne było wykorzystanie czujnika, który mierzyłby położenie kulki na platformie.
Rozważano kamerę oraz panel rezystancyjny. Wykorzystanie kamery wiąże się z koniecznością implementacji analizy obrazu. Należałoby wykorzystać mikrokontroler o dużej mocy obliczeniowej lub analizę obrazu wykonywać na komputerze, a obliczoną pozycję wysyłać do mikrokontrolera. Krytyczny jest tutaj czas w jakim analiza obrazu jest wykonywana, gdyż po każdym pomiarze położenia kulki należy wykonać obliczenia regulatora. Zbyt rzadkie pomiary mogą okazać się niewystarczające by skutecznie regulować położeniem kulki.
Zdecydowano się wykorzystać panel rezystancyjny, który umożliwia szybkie pomiary w porównaniu do analizy obrazu.

#### Regulator

Rozważano różne regulatory: PID, nadążny i krokowy. Zdecydowano się wykorzystać regulator PID, ponieważ jest on stosunkowo prosty w impelementacji.

#### Automatyzacja ustawiania nastaw i zbierania pomiarów

Zbieranie pomiarów jest potrzebne by zbierać dane, a następnie je analizować. Może to dostarczyć informacji o szumach pomiarowych bądź o innych problemach w urządzeniu. Należało również wybrać sposób ustawiania nastaw regulatorów, tak by nie było konieczne kolejne wgranie programu do mikrokontrolera i jego restart przy zmianie nastaw. Zdecydowano się napisać interfejs GUI, który komunikowałby się z mikrokontrolerem za pośrednictwem frameworka ROS. Zdecydowano się wykorzystać framework ROS ze względu na prostotę implementacji komunikacji mikrokontroler-komputer.

### Założenia prototypu

* __Termin oddania__: 5.12.2020
* __Maksymalny koszt__: 150zł
* __Działanie:__
	- Układ utrzymuje obiekt w stabilnej pozycji
	- Układ reaguje na zakłócenia
	- Układ ustawia obiekt w zadanej pozycji
	- Mikrokontroler komunikuje się z komputerem za pomocą frameworka ROS (Robot Operating System) - nastawy regulatorów, informacje o położeniu kulki.
* __Dodatki__
	- Software - aplikacja pozwalająca na wygodne wysyłanie nastaw regulatora oraz zadanej pozycji. Wyświetla aktualną pozycje obiektu oraz uchyb, w sposób graficzny lub liczbowy. Rysuje odpowiedź układu na wykresie lub zapisuje dane w pliku CSV.


### Harmonogram
* 12.09.2020 - Research i projekt prototypu
* 19.09.2020 - Zamówienie części, budowa modelu
* 26.09.2020 - Integracja części z mikrokontrolerem
* 2.10.2020 - Składanie modelu
* 9.10.2020 - Implementacja software-u
* 16.10.2020 - Testy i naprawa błędów
* 23.10.2020 - Testy właściwa regulacja
* 30.10.2020 - Poprawa działania (zastosowanie filtru Kalmana)


### Podział obowiązków
* Odpowiedzialny za Dokumentacje - __Dawid Pysz__
	* Krysian Loranc
	* Dawid Pysz
* Odpowiedzialny za ROS - __Krystian Loranc__
	* Krysian Loranc
	* Dawid Pysz
* Odpowiedzialny za PID - __Krystian Loranc__
	* Krysian Loranc
	* Dawid Pysz
* Odpowiedzialny za Hardware - __Dawid Pysz__
	* Krysian Loranc
	* Dawid Pysz
* Odpowiedzialny za Firmware - __Krystian Loranc__
	* Krysian Loranc
	* Dawid Pysz
* Odpowiedzialny za Software - __Dawid Pysz__
	* Krysian Loranc
	* Dawid Pysz

### Instrukcja/Integracja
Po pierwsze należy zasilić urządzenie napięcem 6V oraz podłączyć je do komputera. Urządznie nie uruchomi się jeśli nie wykryje komunikacji z systemem operacyjnym. Początkowe nastawy są zerowe, więc nie zauważymy na początku żadnej regulacji. Nalęży w tym celu ustawić parametry. Wiązało się to z dostoswaniem położenia serwa względem wału kardana.


```
rostopic pub <NazwaTematu> <TypWiadomiści> <Wiadomość>

Przykład:

rostopic pub balancerTuning balancer/value
"header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
code: 110
x: 0.8
y: 0.8"
```
| Kod | Opis |
|-----|------|
| 100 | Ustawia zadaną pozycję |
| 110 | Ustawia nastawę członu P |
| 115 | Ustawia nastawę członu I |
| 120 | Ustawia nastawę członu D |


Podobnie można zmienić zadaną pozycję, domyślnie jest ona ustawiona na (0,0). Gdy ustawiliśmy już nastawy regulator, to po położeniu obiektu na platformie. Platforma zacznie się poruszać. Należy pamiętać ze obiekt musi być odpowiedniej cieżkości. Jeśli natomisat obiekt spadnie lub zostanie zabrana, platforma ustawi sie pozimo do podstawy.



### Parametry
| Parametr 					| Wartość |
|---------------------------|-------------|
| Wymiary platformy			| 355mmx288mm |
| Wymiary podstawy			| 570mmx370mm |
| Wymagana masa kulki		| >68g |
| Napięcie zasilania		| 6V |
| Maksymalny pobór prądu	| 2A |
| Maksymalne wychylenie		| 20 st|
| Złącze komunikacyjne		| USB typ B |
| Komunikacja 				| ROS |

# Projekt

### Zasada działania
Komunikacja ze sterownikiem odbywa się za pomcą frameworku ROS(Robot Operating System). System wysyła do sterwonika sygnały: zezwolenie na prace(Enable), nastawy regulatora PID dla poszczególnych osi(setPID), pozycje zadaną(setPos). Nastomiast sterwonik odpowida czy udało się odberać nastawy oraz co cykl wysyła aktualną pozycję obiektu na platformie. Sterownik za pomocą klasy Port odbiera i przechowuje odebrane dane oraz pobiera i wysyła aktualną pozycje obiektu. Klasa Panel ma za zadanie odczyt i przeskalowanie pozycji obiektu na panelu rezystacyjnym. Regulator PID osobno dla poszczególnych osi, co cykl, oblicza kąt platformy. Klasa Motor jest odpowiedzalny za sterowanie elementem wykonawczym. Otrzymuję on kąt platformy a następnie oblicza kąt położenia elemntu wykonawczego i realizuje nastawę.

### Graf działania
![Flow Graph](./doc/res/firmware-flowgraph.png)
*Graf działania*

### Model matematyczny
Model układu jest nie liniowy ze względu na to, sinus kąta wpływa napołozenie kulki. Jednak jeśli kąty wychyleń są małe, można zlinearyzować układ. Jest też jeszcze jedna nie liniowość w projekcie, mianowicie przkładnia mechaniczna określająca wpływ kąta serwomechanizmu na kąt platformy. Dlatego zdecydowaliśmy się proacować na zakresach w których platforma jest liniowa.

![Axis Model](./doc/res/axis-model.png)
*Poglądowy model osi*
![Axis Gear X](./doc/res/axis-gear-x.png)
*Przekładnia zmian kąta serwomechanizm do kąta platformy dla osi X*

*Kąt orczyka i platformy, oś X. Jak widać funkcja wychylenia orczyka względem kątu platformy jest nieliniowa dla dużych wychyleń. Należy pracować w zakresie +/- 20 stopni. Dodatkowo należało uwzględnić, że serwomechanizm ma zakres pracy 0->180 stopni*


parametry:
* m - masa kulki
* r - promień kulki
* x - położeie na osi x
* y - położenie na osi y
* ð - kąt platformy wokuł osi x
* ß - kąt platformy wokuł osi y
* T - tarcie toczne $`2mr^2/3`$

Równania dynamiczne:
```math
0 = ( m + T / r ) dx^2/d^2t + m g sin(ß) - m x dx/dt + T/r dß^2/d^2t
```
```math
0 = ( m + T / r ) dy^2/d^2t + m g sin(ð) - m x dy/dt + T/r dð^2/d^2t
```

Równania po zlinearyzowaniu:
```math
dx^2/d^2t = -5/3 g sin(ß_0)
```
```math
dy^2/d^2t = -5/3 g sin(ð_0)
```

Transmitacje po linearyzacji:
```math
K_x = Y_x(s)/U_x(s) = 3g/5s^2
```
```math
K_y = Y_y(s)/U_y(s) = 3g/5s^2
```

### Spis elementów i kosztorys
| Lp | Element 				| Cena    | Ilość | Koszt   |
|----|----------------------|---------|-------|---------|
| 1  | Arduino Uno (klon)	| 18,50 zł|   1   | 18,50 zł|
| 2  | Panel rezystacyjny 	| 27,48 zł|   1   | 27,48 zł|
| 3  | Serwomechanizmy 		| 10,35 zł|   2   | 20,70 zł|
| 4  | Hardware 			| 7,42 zł |   1   | 7,42 zł |
|    | 						|         | SUMA: | 74,10 zł|

# Opis elementów

### Panel rezystacyjny

5-pinowy panel rezystancyjny wykorzystywany jest jako czujnik położenia obiektu na platformie. Jest to szybka i skuteczna metoda pomiaru, ponieważ nie wykorzystuje dużej mocy obliczeniowej do określenia pozycji. Przykładowo określanie pozycji za pomocą kamery i analizy obrazu jest czasochłonne.

###### Budowa paneli rezystacyjnych
Działanie paneli rezystacyjnych polega na pomiarze rezystacji na danej osi w punkcie styku. Na szklanej podstawie znajduję sie płyta o stosonkowo dużej rezystywności, na niej naniesiono siatkę izolatorów mających za zadanie odseparowanie płyty od kolejnej warstwy materiału odizolowanej od zewnetrznej strony. Wyróżnia się panele cztero oraz pięcio pinowe. W projekcie wykorzystano panel pięcio pinowy.

![Resistive touch screen build](./doc/res/panel-build.png)
*Budowa panelu rezystancyjnego*

###### Działanie Paleli rezystacyjnych
Pomiar pozycji w panelu rezystacyjnym zarówno 4-pinowym jak i 5-pinowym wykonuje się osobno dla każdej z osi. W panelu 4-pinowym najpierw ustawia się elektrody, w osi którą mierzymy. Jedną w stan wysoki, a drugą na stan niski. Jedną z elektrody przeciwnej osi, łączymy do masy, a drugą podłączamy do przetwornika ADC. Jeśli obiekt naciśnie panel, czyli złaczy płyty obu osi, powstanie dzielnik napięcia w którym mierzone napicie zmienia się zależnie od miejsca styku na danej osi. Analogidznie wykonywany jest pomiar drugiej osi.

W przypadku 5-pinowych paneli pomiar jest wykonywany podobnie. Różnica polega na ustawienu stanów na poszczególnych elektrodach. By zmierzyć napięcie na osi należy dwie sąsiednie elektrody ustawić w stan wysoki, a przeciwne w niski, tworząc gradient napięcia. Jeśli płyty złączą się w określonym miejscu, spadek napięcia zostanie odczytany przez elektrodę pomiarową podłaczoną do przetworniak ADC.

##### Kompensacja nieliniowości w panelach 5-pinowych
Gdyby w 5-pinowym panelu pozostawić podłączenia na rogach, gradnient rezystacji byłby nielinowy. Utrudnia to znacznie obliczenia związanie z przeskalowaniem napięcia na pozycję. Dlatego też stosuje się wzór na krawędziach wykonany z przewodnika (Linearization Pattern). Powoduje on znaczne zmniejszenie się rezystacji na bokach panelu. Efektem tego jest kompesacja nielinowiści panelu, a zasadą działana przypomina panel 4-pinowy.

![Resistive touch screen connect](./doc/res/panel-connect.png)
*Podłącznienia panelu rezystancyjnego*

##### Implematacja odczytu
Odczyt położenia odbywa sie w kilku krokach. Pierwszym z nich jest ustawienie stanów na elektrodach dla osi X lub Y zgodnie z poniższą tabelą. Jeśli pozycja ma zostać odczytana dokładnie należy odczekać chwilę, zamim odczyta się wejście analogowe. Opóźnienie zapewnia, że napięcie w czasie odczytu będzie napięciem stabilnym. Ponieważ panel rezystacyjny posiada pojemność pasożytniczą opóźniającą narastanie napięcia. Po odczekaniu, odczytwana jest wartość analogowa. A następnie napięce jest przeliczane na pozycję.

![Resistive touch screen scan 5pin](./doc/res/panel-scan-5pin.png)
*Tabela nastaw do odczytu, wersja 5-pin*
![Resistive touch screen scan 4pin](./doc/res/panel-scan-4pin.png)
*Tabela nastaw do odczytu, wersja 4-pin*
![Resistive touch screen pinout](./doc/res/panel-pinout.png)
*Panel rezystacyjny pinout*

### Arduino Uno Rev3
Mikrokontroler jest głównym elementem sterującym. Do jego zadań należy odczyt wartości z portu komunikacyjnego, regulacja PID, odczty pozycji z panelu oraz sterowanie serwomechanizmami. Wykorzystje 6 wyjśc cyfrowych w tym 2 PWM, jedno wejście analogowe, interfejs komunikacyjny oraz całą dostępną pamięć RAM.

##### Możliwości mikrokontrolera
Arduino UNO jest zbudowane na podstawie mikrokontrolera ATMEGA328P, jest to 8-bitowy, jednordzeniowy procesor z rodziny AVR, o architekturze RISC. ATmega ma wydajność 1DMIPS, a jej taktowanie oficjalnie moze być podkręcone do 16MHz, co oznacza, że jej moc obliczeniowa może wynosić 16MIPS. Posiada 32 kB pamięci Flash, 2 kB RAM. 14 Wejść/Wyjść cyfrowych z czego 6 może być wykorzystywana jako wyjście PWM oraz 6 wejść analogowych. Interfejsy komunikacyjne to I2C (Inter-Integrated Circuit) oraz SPI (Serial Peripheral Interface), posiada również port szeregowy USB typu B.

![Arduino pinout](./doc/res/arduino-pinout.png)
*Arduino pinout*

### Serwomechanizmy
Serwomechanizmy pełnią rolę obiektu wykonawczego, sterują wychyleniem platformy. W projekcie do serwomechaznimów doczepiono plastikowe orczyki i przykręcono je śrubkami. Orczyki połączono z popychaczami. Na popychacze z drugiej strony wkręcono snapy z przegubami kulowymi. Snapy te przykręcono do kątowników przykręconych do platformy.

#### Zasada działania
Serwomechanizmy są to silniki pracujące w pętli zamkniętej. Serwomechanizmy modelarskie najczęściej składają się z:
- silnika prądu stałego
- potencjometru
- układu elektronicznego sterującego silnikiem
- przekładni zębatej

Potencjometr pełni rolę czujnika położenia kątowego osi serwa. Serwomechanizmy modelarskie są najczęścień 3 przewodowe. Dwa piny to zasilanie, a trzeci to sygnał. Aby wysterować serwomechanizmem należy mu wysłać sygnał PWM. Do serwomechanizmach modelarskich stosowany jest nietypowy sygnał PWM. Jest to impuls generowany co 20 ms (częstotliwość 50 Hz), który ma trwać od 800-2200 µs. Czas trwania impulsu mówi serwomechanizmowi w jakiej pozycji ma się znaleźć. Serwomechanizmy modelarskie najczęściej mają zakres pracy 0-180°. Impuls trwający 800 µs powoduje wysterowanie serwomechanizmu do skrajnej pozycji 0°, natomiast impuls trwający 2200 µs powoduje wysterowanie do pozycji 180° (obrót zgodnie z wskazówkami zegara).

##### Pinout
| Pin | Kolor |
|-----|-------|
| GND | brązwoy |
| VCC | czerwony |
| PWM | pomarańczowy |

# Hardware

![Hardware image](./doc/res/hardware-img.png)
*Zdjęcie elektroniki*

### Schemat elektroniczny
![Datasheet circuit](./doc/res/hardware-circuit.png)
*Schemat elektroniczny*

### BOM
![Hardware BOM](./doc/res/hardware-BOM.png)
*Lista elementów*

# Firmware

### Robot Operating System
Jest to platforma programistyczna typu open-source, która pozwala na stosunkowo proste tworzenie oprogramowania sterowania robotów. Zawiera wiele bibliotek pozwalających na sterowanie i symulację pracy robota. Jest wykorzystywany między innymi w projektach realizowanych przez koła naukowe oraz hobbystów. Aktualnie rozwijana jest również wersja ROS Industrial mająca na celu rozwój tej platformy w kierunku przemysłu.
ROS zapewnia komunikację pomiędzy procesami oraz pozwala na stosunkowo proste stworzenie rozproszonej struktury procesów, zwanych węzłami. Różne procesy mogą być napisane w różnych językach programowania, a mimo to i tak będą mogły komunikować się ze sobą. ROS zapewnia duży poziom abstrakcji sprzętowej jak i programistycznej.

##### Nodes
Węzły (z ang. nodes) są to główne składowe systemu ROS. Węzeł to plik wykonywalny, który używa ROSa do komunikacji z innymi węzłami. Węzły mogą nadawać wiadomości lub nasłuchiwać wiadomości na danym temacie (ang. topic). ROS został zaprojektowany jako modułowy system w skali drobnoziarnistej. System sterowania robotem zwykle składa się z wielu węzłów. Przykładowo jeden węzeł może być odpowiedzialny za sterowanie silnikami, inny węzeł odpowiedzialny za analizę obrazu, kolejny za komunikację z urządzeniem zewnętrznym, kolejny węzeł może być odpowiedzialny za planowanie ścieżki itd.

##### Master
ROS Master zapewnia rejestrację nazwy węzła oraz wymianę wiadomości i wywoływanie serwisów.

##### Messages
Wiadomości to typy danych w ROSie, które są używane do komunikacji pomiędzy węzłami.
Istnieją różne typy wiadomości, każda zawiera zdefiniowaną strukturę. Możliwe jest również tworzenie własnych wiadomości składających się z różnych typów danych.
Przykład:
```
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
float64 temperature
float64 variance
```
Jest to wiadomość z paczki sensor_msgs/Temperature, składa się ona z nagłówka, który zawiera informacje takie jak chwila czasowa, frame_id jest to lokacja skąd temperatura jest czytana oraz zawiera pomiar temperatury zapisany jako float64. Powyższy kod to również definicja wiadomości.

##### Topics
Wiadomości są publikowane przez węzły na danym temacie (ang. topic). Temat to nazwa używana do identyfikowania treści wiadomości. Węzeł, który jest zainteresowany odbiorem określonych danych subskrybuje odpowiedni temat. Węzeł wysyłający dane to publisher, a odbierający to subscriber. Na jednym temacie może publikować wiadomości wiele węzłów, a także wiele węzłów może je odbierać.

##### Services
Serwisy zapewniają komunikację na zasadzie żądania i odpowiedzi.

### Regulator PID
W celu sterowania obiektem na platformie napisano algorytm regulacji PID.
Regulator ten składa się z 3 członów - proporcjonalne, całkującego i różniczkowego.
Wartości obliczane przez każdy z członów są sumowane i na ich podstawie wysterowywany jest element wykonawczy. W naszym przypadku jest to serwomechanizm.
Człon proporcjonalny przemnaża uchyb przez współczynnik proporcjonalności (kp).
Człon integrujący sumuje uchyby w każdym cyklu obliczeń regulatora i przemnaża je przez współczynnik całkowania (ki).
Człon różniczkującyprzemnaża różniczkę uchybu przez współczynnikkd.
Uchyb stanowi różnica pomiędzy wartością zadaną, a wartością aktualną położenia kulki.
Różniczkę stanowi różnica wartości aktualnej i wartości odczytanej w poprzednim cyklu obliczeń regulatora.

Jako, że nasz mechanizm ma pewne ograniczenia mechaniczne zastosowano ograniczenia softwarowe by zapobiec uszkodzeniu elementów.
By regulator PID lepiej działał i nie wychodził zbyt długo ze stanu nasycenia wykorzystano anti-windup.


# Software
Jako dodatek uwzględniliśmy applikacje, która komunikuje się z urządzeniem za pomocą ROSa. Ma ona przyjemny interface, który pozwala na dobór nastaw.

![GUI Software](./doc/res/software-gui.png)
*Software: interface*
### Diagram klas
![Class Diagram](./doc/res/software-class-diagram.png)
*Software: zależności między klasami*

| Klasa | Opis |
|-------|------|
| __Interface__ | Klasa zarządzająca, posiadająca niezbędne zasoby, odpowiada za inicjalizacje GUI.|
| __Sygnały__ | Klasa odpowida za obsługę zdarzeń takich jak przyciski, suwaki.|
| __RosConnect__ | Odpowiada za obsługe komunikacji z urządzeniem.|
| __ToCSV__ | Zapisjue zawartośc scope do pliku csv.|
| __Chart__ | Wykreśla wykres na ekranie.|
| __Stalker__ | Sledzi połozenie obiektu i przedstawia je na ekranie.|
| __Scope__ | Monitoruje dany parametr, zapisując jego przebieg.|
| __Drawable__ | Klasa odpowiedzalna za współdzelenie zasobu ekranu.|


# Napotkane problemy

- Kolidowanie kątownika z popychaczem serwomechanizmu przy dużych przechyleniach.
	* __Rozwiązanie:__ Kątownik przycięto.
- Mikrokontroler odczytywał napięcie panelu rezystancyjnego, mimo, że nic nie dotykało panelu rezystancyjnego.
	* __Rozwiązanie:__ W celu rozwiązania problemu wykorzystano rezystor pull-down.
- Dziwne zachowania dwóch serwomechanizmów w trakcie ich sterowania. Oba serwomechanizmy były zasilane z zasilacza laboratoryjnego. Problemem okazał się być mikrokontroler, który miał za mało prądu do wykorzystania dwóch sygnałów PWM.
	* __Rozwiązanie:__ W celu rozwiązania problemu Arduino zasilono z dodatkowego źródła.
- Sporadyczne występowanie dziwnego zachowania serwomechanizmów. Problem stanowiło testowanie układu elektronicznego na płytce stykowej.
 	* __Rozwiązanie:__ Wykonanio hardware na płycie PCB.
- Zakłócenia odczytu panelu.
	* __Rozwiązanie:__ Należy ustawić opóźnienie przed odczytem wartości napięcia na panelu.
- Problemy firmwarowe spowodowane błędami w algorytmie regulatora, podwójnym przeliczaniem przełożenia kątów serwa i platformy
	* __Rozwiązanie:__ Zaimplemnwtowanie przeliczania tylko w jedej klasie.
- Problemy softwarowe - problem z integracją frameworku ROS z gui.
	* __Rozwiązanie:__ Zastoswanie funkcji statycznej oraz wykonywanie jej jako zadnie Idle.
- W przypadku małej kulki przy szybkich ruchach platformy kulka odrywa się od niej, przez co panel nie wykrywa jej obecności, a co za tym idzie regulator szarpie platformą ponieważ próbuje ustawić ją poziomo.
	* __Rozwiązanie:__ Zastosowanie bufora, który ignorje N próbek gdy kullka jest oderwana. Dopiero po N próbkach następije wykrycie braku obiektu na platformie.

# Podsumowanie
Udało się zrealizować określone założenia, kukla utrzymuje się na platformie oraz zmienia zadaną pozycję. Dodatek w postaci softwaru znacznie ułatwił testowanie oraz zautomatyzował zbieranie danych. Dzięki tym danym, wykreśliliśmy wykresy, które ukazały kolejne problemy zwizane z jakością regulacji. Zastosowanio między innymi buffor ignorujący oderwanie kulki od panelu oraz filtr Kalmana. W dalszej częsci usprawnień można zaimplementować ignorowanie błedów grubych.


![Test-1](./doc/res/test-1.png)
*Wykres testu-1*



![Test-1](./doc/res/test-2.png)
*Wykres testu-2*

# Autorzy
- Krystian *KryKryKry* Loranc
- Dawid *Dejv* Pysz

# Literatura

* [Artykuł o sterowaniu stacją radioleokacyjnej - 12.2020](http://www.obrum.gliwice.pl/upload/downloads/spg/108/11_Barcik.pdf)
* [Dokumentacja mikrokontrolera ATMEGA328P - 12.2020](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)
* [AVR341: Four and five-wire Touch Screen Controller - 12.2020](http://ww1.microchip.com/downloads/en/AppNotes/doc8091.pdf)
* [ROS wikipedia - 12.2020](https://pl.wikipedia.org/wiki/Robot_Operating_System)
* [ROS concept - 12.2020](http://wiki.ros.org/ROS/Concepts)
* [ROS - 12.2020](http://wiki.ros.org/)
* [Serwomechanizmy - 12.2020](http://abc-modele.pl/serwomechanizmy/)
* [Arduino documentary reference - 12.2020](https://www.arduino.cc/reference/en/)
* [Cairo tutorial - 12.2020](https://www.cairographics.org/tutorial/)
* [GTK3++ documentary reference - 12.2020](https://developer.gnome.org/gtk3/stable/)
* [C++ documentary reference - 12.2020](https://www.cplusplus.com/reference/)
