classDiagram

class Interface

class RosConnect
class Signal
class Stalker
class ToCSV
class Chart
class Scope

class Drawable

Interface <--> Signal
Interface --o ToCSV
Interface --o Chart

Chart <-- Scope
ToCSV <-- Scope

Interface --o Scope
Interface --o Stalker

Stalker <|-- Drawable
Chart <|-- Drawable

Interface --o RosConnect
